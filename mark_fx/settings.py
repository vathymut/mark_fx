# Scrapy settings for mark_fx project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'mark_fx'

SPIDER_MODULES = ['mark_fx.spiders']
NEWSPIDER_MODULE = 'mark_fx.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'mark_fx (+http://www.yourdomain.com)'
