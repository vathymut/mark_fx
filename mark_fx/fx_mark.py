# -*- coding: utf-8 -*-
"""
Created on Sunday January 12, 2014

@author: Vathy
"""

# from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.spider import BaseSpider
from thebrick.items import ThebrickItem
from unidecode import unidecode
from time import sleep

                    
# Declare StartUrl and PatternToFollow as constants
STARTURL = "http://www.forexite.com/free_forex_quotes/forex_history_arhiv.html"
URLSTUMP = "http://www.forexite.com/free_forex_quotes/"

class TheBrickSpider(BaseSpider):
    '''
    Spider to extract FSA Codes for most Canadian cities.
    '''
    name = "fx_mark"
    allowed_domains = ["forexite.com"]
    start_urls = [ STARTURL ]
	
    def parse(self, response):
		"""
		Get all links to store location info
		from thebrick.com website.
		"""
		self.log( 'A response from %s just arrived!' % response.url )
		hxs = HtmlXPathSelector(response)
		loc = hxs.select("//div[@align = 'right']/font/a/@href").extract()
		with open( 'fx_first_page_links.txt', 'w') as f:
			for l in loc:
				if l.endswith( '.zip' ): 
					f.write( URLSTUMP + l + '\n' )
					print l
					yield Request( URLSTUMP + l, callback = self.download_zip )
		other_pages = hxs.select("//p/a/@href").extract()
		for l in other_pages:
			if 'forex_history_arhiv' in l:
				print l
				yield Request( URLSTUMP + l, callback = self.download_other_zip)
			
    def download_zip( self, response ):
	    '''
	    Do nothing, dummy. It simply downloads the .zip files.
	    '''
	    self.log( 'A response from %s just arrived!' % response.url )
	    pass
		
    def download_other_zip( self, response ):
	    '''
	    Fine. Now do something.
        '''
	    self.log( 'A response from other %s just arrived!' % response.url )
	    hxs = HtmlXPathSelector(response)
	    loc = hxs.select("//div[@align = 'right']/font/a/@href").extract()
	    with open( 'fx_other_page_links.txt', 'a+' ) as f:
		    for l in loc:
			    if l.endswith( '.zip' ): 
				    f.write( URLSTUMP + l + '\n' )
				    print l

