# -*- coding: utf-8 -*-
"""
@author: Vathy M. Kamulete

"""
# For dealing with web
from urlparse import urljoin, urlsplit
from urllib2 import urlopen, Request
# Other utilities
from time import sleep
import shutil
import datetime
from fnmatch import filter
from os import chdir, listdir, getcwd, makedirs
from os.path import exists, basename, splitext, dirname, abspath

# Get directory where this file is running from
HOMEDIR = dirname( abspath(__file__) )
chdir( HOMEDIR )

# Get all files
all_filenames = listdir( HOMEDIR ) 

# Keep all .txt files
pattern = 'fx*.txt'
TXT_FILENAMES = filter( all_filenames, pattern )
		
def get_links_from_file( filename ):
	'''
	Get links/urls from the filename
	'''
	with open( filename, 'r') as f:
		for url in f:
			yield url
		
def give_filename( url_rel ):
    '''
    Give the pdf a filnename.
	Append time information to the filename.
    '''
    filename = basename( url_rel )
	# Add time information
    now_datetime = datetime.datetime.now( )
    now_string = now_datetime.strftime( "%Y-%m-%d-%H-%M-%S" )
    if filename.endswith( '.zip' ):
		fileno, ext_zip = splitext( filename )
		zip_filename = fileno + '-' + now_string + ext_zip
		return zip_filename
	
def download_zip( url, filename = None ):
    '''
    Download and name the pdf file
    '''
    r = urlopen( Request( url ) )
    try:
        if filename is None:
            filename = give_filename( url )
        with open( filename, 'wb' ) as f:
            shutil.copyfileobj( r, f )
    finally:
        r.close()
			
def get_filename_and_url( filenames ):
	'''
	Transform nested for loop into 1D loop.
	Returns tuple of filename and url being iterated over.
	'''
	for filename in filenames:
		urls_gen = get_links_from_file( filename )
		for url in urls_gen:
			 yield ( filename, url )

def create_dir( d ):
	'''
	Create directory if it does not exist already.
	Taken from StackOverflow:
	http://stackoverflow.com/q/273192/1965432
	'''
	if not exists( d ):
		makedirs( d )
		return True
	else:
		return False
		
#### Test
if __name__ == '__main__':
	try:	
		# Change this line if saving elsewhere
		mark_dir = create_dir(  r'B:\mark_zip' ) 
		# Get files already downloaded
		chdir( r'B:\mark_zip' ) 
		current_filenames = listdir(  getcwd() )
		current_filenames = [ f.split('-')[0] for f in current_filenames ]
		# Go to where this file is running from
		chdir( HOMEDIR )
		# Set counter
		count_no = 0
		for filename, url in get_filename_and_url( TXT_FILENAMES ):
			print 'Current directory', getcwd()
			print 'Filename:', filename
			url = url.rstrip()
			print 'Url:', url
			zip_filename = give_filename( url_rel = url )
			# Download the pdf
			trimmed_zip_filename = zip_filename.split('-')[0]
			if trimmed_zip_filename in current_filenames:
				print 'Already downloaded\t trimmed zip filename:', trimmed_zip_filename, '\n'
				count_no += 1
				continue
			try:
				chdir( r'B:\mark_zip' ) 
				download_zip( url, zip_filename )
				print 'Attempt succeeded in directory:' , getcwd()
				print 'File saved as:', zip_filename, '\n'
			except URLError as err:
				print 'some error'
				pass
			chdir( HOMEDIR )
	finally:
	   chdir( HOMEDIR )
	   print 'Number of files downloaded', count_no
	

